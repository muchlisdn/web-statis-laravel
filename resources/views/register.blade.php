<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<h2>Buat Account Baru!</h2>
	<h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
		<label>First name:</label><br>
		<input type="text" name="firstname"><br>
		<label>Last name:</label><br>
		<input type="text" name="lastname"><br>
		<label>Gender:</label><br>
		<input type="radio" name="gender">Male <br>
		<input type="radio" name="gender">Female <br>
		<input type="radio" name="gender">Other <br>
		<label>Nationality</label><br>
		<select name="nation">
			<option>Indonesian</option>
			<option>English</option>
			<option>Malaysian</option>
			<option>Australian</option>
		</select>
		<br>
		<label>Language Spoken:</label><br>
		<input type="checkbox" name="language">Bahasa Indonesia <br>
		<input type="checkbox" name="language">English <br>
		<input type="checkbox"name="language">Other <br>
		<label>Bio:</label><br>
		<textarea name="bio"></textarea>
		<br>
		<input type="Submit" value="Sign Up">
	</form>

</body>
</html>