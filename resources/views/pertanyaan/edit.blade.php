@extends('templates.master')
@section('content')
<h1>Edit Pertanyaan</h1>
<form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="judul">Judul</label>
      <input type="text" class="form-control" id="judul" name="judul" value="{{$pertanyaan->judul}}" placeholder="Enter Judul">
        @error('judul')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      </div>
      <div class="form-group">
        <label for="isi">Isi</label>
        <input type="text" class="form-control" id="isi" name="isi" value="{{$pertanyaan->isi}}" placeholder="Isi">
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
          
      @enderror
    </div>
      <div class="form-group">
        <label for="tanggal_dibuat">Tanggal Dibuat</label>
        <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat" value="{{$pertanyaan->tanggal_dibuat}}" placeholder="Tanggal Dibuat">
        @error('tanggal_dibuat')
      <div class="alert alert-danger">{{ $message }}</div>
          
      @enderror
      </div>
      <div class="form-group">
        <label for="tanggal_diperbarui">Tanggal Diperbarui</label>
        <input type="date" class="form-control" id="tanggal_diperbarui" name="tanggal_diperbarui" value="{{$pertanyaan->tanggal_diperbarui}}" placeholder="Tanggal Diperbarui">
        @error('tanggal_diperbarui')
      <div class="alert alert-danger">{{ $message }}</div>
          
      @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Kirim</button>
    </div>
  </form>
@endsection