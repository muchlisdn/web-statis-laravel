@extends('templates/master')
@section('content')
<h1>Pertanyaan</h1>
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
      @if(session('success'))
      <div class="alert alert-success">
        {{session('success')}}
      </div>
      @endif
        <a href="/pertanyaan/create" class="btn btn-info mb-2">Buat Pertanyaan Baru</a>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>No</th>
          <th>Judul</th>
          <th>Isi</th>
          <th>Tanggal Dibuat</th>
          <th>Tanggal Diperbarui</th>
          <th style="width:40px">Action</th>

        </tr>
        </thead>
        <tbody>
        @forelse ($pertanyaan as $key => $tanya)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$tanya->judul}}</td>
                <td>{{$tanya->isi}}</td>
                <td>{{$tanya->tanggal_dibuat}}</td>
                <td>{{$tanya->tanggal_diperbarui}}</td>
                <td>
                    <a href="/pertanyaan/{{$tanya->id}}" class="btn btn-default">show</a>
                    <a href="/pertanyaan/{{$tanya->id}}/edit" class="btn btn-warning">edit</a>
                <form action="/pertanyaan/{{$tanya->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
                </td>
            </tr>
            @empty
                <tr>
                    <td colspan="6" align="center">No Data</td>
                </tr>
            
        @endforelse
        </tbody>
        
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection