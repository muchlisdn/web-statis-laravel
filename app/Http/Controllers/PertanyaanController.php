<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }
    public function create(){
        return view('pertanyaan.create');
    }
    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan,judul',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbarui' => 'required',
        ]);
        $query = DB::table('pertanyaan')->insert([
        "judul" => $request["judul"],
        "isi" => $request["isi"],
        "tanggal_dibuat" => $request["tanggal_dibuat"],
        "tanggal_diperbarui" => $request["tanggal_diperbarui"],
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }
    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }
    public function update($id, Request $request){
        // dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan,judul',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbarui' => 'required',
        ]);
        $query = DB::table('pertanyaan')->update([
        "judul" => $request["judul"],
        "isi" => $request["isi"],
        "tanggal_dibuat" => $request["tanggal_dibuat"],
        "tanggal_diperbarui" => $request["tanggal_diperbarui"],
        ]);
        return redirect('/pertanyaan');
    }
    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        // dd($pertanyaan);
        return view('pertanyaan.show', compact('pertanyaan'));
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
