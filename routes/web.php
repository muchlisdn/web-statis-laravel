<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
Route::get('/data-tables', 'HomeController@datatables');

Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan/store', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan}/edit', 'PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan}', 'PertanyaanController@destroy');
